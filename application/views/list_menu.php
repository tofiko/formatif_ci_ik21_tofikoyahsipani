<head>
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/style.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-grid.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-grid.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-reboot.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-reboot.min.css">
</head>

<body>
<div class="content">
	<div class="menu">
		<ul>
        	<li><a href="<?=base_url();?>karyawan/listkaryawan">KARYAWAN</li></a>
        	<li><a href="<?=base_url();?>menu/listmenu">| MENU |</li></a>
            <li><a href="<?=base_url();?>pemesanan/listpemesanan">PEMESANAN</li></a>
    	</ul>	
	</div>
    <div class="badan">
    
<div><h1 align="center">Data Menu</h1></div>
<div>
<h4 align="right" style="margin-right:2%; padding-bottom:5px; font-size:20px;">
<a href="<?=base_url();?>menu/input">[Tambah Data]</a>
</h4>
</div>
<table align="cenetr" width="100%" border="1" cellspacing="0" cellpadding="10">
 <tr style="background-color:#03F; color:#FFF;">
 	<td>No</td>
    <td>Kode Menu</td>
    <td>Nama Menu</td>
    <td>Harga</td>
    <td>Keterangan</td>
    <td></td>
 </tr>
 <?php
 	$no=0;
	foreach ($data_menu as $data)
	{$no++;
 ?>
 <tr>
 	<td><?=$no;?></td>
    <td><?=$data->kode_menu;?></td>
    <td><?=$data->nama_menu;?></td>
    <td><?=$data->harga;?></td>
    <td><?=$data->keterangan;?></td>
    <td>
        <a href="<?=base_url();?>menu/editmenu/<?=$data->kode_menu;?>">Edit</a> 
    	<a href="<?=base_url();?>menu/deletmenu/<?=$data->kode_menu;?>" onClick="return confirm('Yakin Ingin Dihapus')">| Delete</a> 
    </td>
 </tr>
 <?php
	}
 ?>
</table>
 </div>
</div>
</body>
</html>