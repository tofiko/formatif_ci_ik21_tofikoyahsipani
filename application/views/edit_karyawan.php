<head>
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/style.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-grid.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-grid.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-reboot.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-reboot.min.css">
</head>

<body>
<div class="content">
	<div class="menu">
		<ul>
        	<li><a href="<?=base_url();?>karyawan/listkaryawan">PEGAWAI</a></li>
        	<li><a href="<?=base_url();?>menu/listmenu">| MENU |</a></li>
            <li><a href="<?=base_url();?>pemesanan/listpemesanan">PEMESANAN</li></a>
    	</ul>	
	</div>
    
<div class="badan">
<h1 align="center" style="font-family:'Arial Black';">EDIT DATA KARYAWAN</h1>
<?php
	foreach($edit_karyawan as $data)
	{
		$nik			= $data->nik;
		$nama			= $data->nama;
		$alamat			= $data->alamat;
		$telp			= $data->telp;
		$tempat_lahir	= $data->tempat_lahir;
		$tgl_lahir 		= $data->tanggal_lahir; 	
	}
	$thn_pisah = substr($tgl_lahir,0,4);
	$bln_pisah = substr($tgl_lahir,5,2);
	$tgl_pisah = substr($tgl_lahir,8,4);
?>
<form method="POST" action="<?=base_url();?>karyawan/editkaryawan/<?=$nik;?>">
<table width="50%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#00FFFF">
	<tr>
    	<td>NIK</td>
        <td>:</td>
        <td>
        	<input type="text" name="nik" id="nik" value="<?=$nik;?>" readonly>
        </td>
    </tr>
    
    <tr>
    	<td>Nama Karyawan</td>
        <td>:</td>
        <td>
        	<input type="text" name="nama" id="nama" value="<?=$nama;?>">
        </td>
    </tr>
    
    <tr>
    	<td>Alamat</td>
        <td>:</td>
        <td>
        <textarea name="alamat" id="alamat" cols="45" rows="5"><?=$alamat;?></textarea>
        </td>
    </tr>
    
    <tr>
    	<td>Telepon</td>
        <td>:</td>
        <td>
        <input type="text" name="tlp" id="tlp" value="<?=$telp;?>">
        </td>
    </tr>
    
    <tr>
    	<td>Tempat Lahir</td>
        <td>:</td>
        <td>
        	<input type="text" name="tempat_lahir" id="tempat_lahir" value="<?=$tempat_lahir;?>">
        </td>
    </tr>
    
    <tr>
    	<td>Tanggal Lahir</td>
        <td>:</td>
        <td>
        <select name="tgl" id="tgl">
        	<?php
            	for($tgl=1;$tgl<=31;$tgl++)
				{
					$select_tgl = ($tgl ==  $tgl_pisah) ? 'selected' : '';
			?>
            	<option value="<?=$tgl;?>" <?=$select_tgl;?>><?=$tgl;?></option>
            <?php
				}
			?>
        </select>
           
        <select name="bln" id="bln">
        	<?php
            	$bulan=array('Januari','Februari','Maret','April',
	   							'Mei','Juni','Juli','Agustus','September',
								'Oktober','November','Desember');
				for($bln=0;$bln<12;$bln++)
				{
					$select_bln = ($bln == $bln_pisah) ? 'selected' : '';
			?> 
            	<option value="<?=$bln+1;?>" <?=$select_bln;?>><?=$bulan[$bln];?></option>
            <?php
				}
			?>  		
        </select>
        
        <select name="thn" id="thn">
        	<?php
            	for($thn=1990;$thn<=date('Y');$thn++)
				{
					$select_thn = ($thn == $thn_pisah) ? 'selected' : '';
			?>
            	<option value="<?=$thn;?>" <?=$select_thn;?>><?=$thn;?></option>
            <?php
				}
			?>
        </select>
        </td>
    </tr>
    
    <tr>
    	<td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>
        	<input type="submit" name="submit" id="submit" value="Simpan">
            <input type="reset" name="reset" id="reset" value="Batal">
        </td>
    </tr>
    
    <tr>
    	<td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>
        <a href="<?=base_url();?>karyawan/listkaryawan">
        	<input type="button" name="submit" id="submit" value="Kembali">
       	</a>
        </td>
    </tr>
</table>
</form>
</div>

</div>
</body>
</html>