<head>
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/style.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-grid.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-grid.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-reboot.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-reboot.min.css">
</head>

<body>
<div class="content">
	<div class="menu">
		<ul>
        	<li><a href="<?=base_url();?>karyawan/listkaryawan">PEGAWAI</a></li>
        	<li><a href="<?=base_url();?>menu/listmenu">| MENU |</a></li>
            <li><a href="<?=base_url();?>pemesanan/listpemesanan">PEMESANAN</li></a>
    	</ul>	
	</div>
    
<div class="badan">
<div><h1 align="center">Data Pegawai</h1></div>
<div>
<h4 align="right" style="margin-right:2%; padding-bottom:5px; font-size:20px;">
<a href="<?=base_url();?>karyawan/input">[Tambah Data]</a>
</h4>
</div>
<table align="center" width="100%" border="1" cellspacing="0" cellpadding="10">
	<tr style="background-color:#03F; color:#FFF">
    	<td>No</td>
        <td>NIK</td>
        <td>Nama Lengkap</td>
        <td>Alamat</td>
        <td>Telp</td>
        <td>Tempat Lahir</td>
        <td>Tanggal Lahir</td>
        <td></td>
    </tr>
    	<?php
        	$no=0;
			foreach($data_karyawan as $data)
			{$no++;
		?>
    <tr>
    	<td><?=$no;?></td>
        <td><?=$data->nik;?></td>
        <td><?=$data->nama;?></td>
        <td><?=$data->alamat;?></td>
        <td><?=$data->telp;?></td>
        <td><?=$data->tempat_lahir;?></td>
        <td><?=$data->tanggal_lahir;?></td>
        <td>
            <a href="<?=base_url();?>karyawan/editkaryawan/<?=$data->nik;?>">Edit</a> 
            <a href="<?=base_url();?>karyawan/deletekaryawan/<?=$data->nik;?>" onClick="return confirm('Yakin Ingin Dihapus')">| Delete</a> 
    	</td>
    </tr>
    	<?php
			}
		?>
</table>
</div>

</div>
</body>
</html>