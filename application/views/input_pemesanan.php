<head>
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/style.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-grid.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-grid.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-reboot.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-reboot.min.css">
</head>

<body>
<div class="content">
	<div class="menu">
		<ul>
        	<li><a href="<?=base_url();?>karyawan/listkaryawan">PEGAWAI</a></li>
        	<li><a href="<?=base_url();?>menu/listmenu">| MENU |</a></li>
            <li><a href="<?=base_url();?>pemesanan/listpemesanan">PEMESANAN</li></a>
    	</ul>	
	</div>
    
<div class="badan">
<form method="POST" action="<?=base_url();?>pemesanan/input">
<h1 align="center" style="font-family:'Arial Black';">INPUT DATA PEMESANAN</h1>
<table width="50%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#00FFFF">
	<tr>
    	<td>Nama Karyawan</td>
        <td>:</td>
        <td>
        	<select name="nik" id="nik">
      			<?php foreach($data_karyawan as $data) 
					{	
				?>
      				<option value="<?= $data->nik; ?>"><?= $data->nama; ?></option>
      			<?php 
					} 
				?>
      			</select>
        </td>
    </tr>
    
    <tr>
    	<td>Tanggal Pemesanan</td>
        <td>:</td>
        <td>
        <select name="tgl" id="tgl">
        	<?php
            	for($tgl=1;$tgl<=31;$tgl++)
				{
			?>
            	<option value="<?=$tgl;?>"><?=$tgl;?></option>
            <?php
				}
			?>
        </select>
           
        <select name="bln" id="bln">
        	<?php
            	$bulan_n=array('Januari','Februari','Maret','April',
	   							'Mei','Juni','Juli','Agustus','September',
								'Oktober','November','Desember');
				for($bln=0;$bln<12;$bln++)
				{
			?> 
            	<option value="<?=$bln+1;?>"><?=$bulan_n[$bln];?></option>
            <?php
				}
			?>  		
        </select>
        
        <select name="thn" id="thn">
        	<?php
            	for($thn=date('Y');$thn<=date('Y')+50;$thn++)
				{
			?>
            	<option value="<?=$thn;?>"><?=$thn;?></option>
            <?php
				}
			?>
        </select>
        </td>
    </tr>
    
    <tr>
    	<td>Nama Pelanggan</td>
        <td>:</td>
        <td>
        	<input type="text" name="nama_pelanggan" id="nama_pelanggan">
        </td>
    </tr>
    
    <tr>
    	<td>Nama Menu</td>
        <td>:</td>
        <td>
        	<select name="kode_menu" id="kode_menu">
      			<?php foreach($data_menu as $data) 
					{	
				?>
      				<option value="<?= $data->kode_menu; ?>"><?= $data->nama_menu; ?></option>
      			<?php 
					} 
				?>
      		</select>
        </td>
    </tr>
    
    <tr>
    	<td>QTY</td>
        <td>:</td>
        <td>
        <input type="text" name="qty" id="qty">
        </td>
    </tr>
    
    <tr>
    	<td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>
        	<input type="submit" name="submit" id="submit" value="Simpan">
            <input type="reset" name="reset" id="reset" value="Batal">
        </td>
    </tr>
    
    <tr>
    	<td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>
        <a href="<?=base_url();?>menu/listmenu">
        	<input type="button" name="submit" id="submit" value="Kembali">
       	</a>
        </td>
    </tr>
</table>
</form>
</div>

</div>
</body>
</html>