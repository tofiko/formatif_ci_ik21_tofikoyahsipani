<head>
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/style.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-grid.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-grid.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-reboot.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-reboot.min.css">
</head>

<body>
<div class="content">
	<div class="menu">
		<ul>
        	<li><a href="<?=base_url();?>karyawan/listkaryawan">KARYAWAN</li></a>
        	<li><a href="<?=base_url();?>menu/listmenu">| MENU |</li></a>
            <li><a href="<?=base_url();?>pemesanan/listpemesanan">PEMESANAN</li></a>
    	</ul>	
	</div>
    <div class="badan">
    
<div><h1 align="center">Data Pemesanan</h1></div>
<div>
<h4 align="right" style="margin-right:2%; padding-bottom:5px; font-size:20px;">
<a href="<?=base_url();?>pemesanan/input">[Input Pemesanan]</a>
</h4>
</div>
<table align="cenetr" width="100%" border="1" cellspacing="0" cellpadding="10">
 <tr style="background-color:#03F; color:#FFF;">
 	<td>Nama Karyawan</td>
    <td>Tanggal Pesanan</td>
    <td>Nama Pelanggan</td>
    <td>Nama Menu</td>
    <td>QTY</td>
    <td>Total Harga</td>
 </tr>
 <?php
	foreach ($data_pemesanan as $datap)
	{
 ?>
 <tr>
 	<td>
	<?php
    	foreach($data_karyawan as $datak)
		{
	?>
		<?=$datak->nama;?>
    </td>
    <?php
		}
	?>
    <td><?=$datap->tgl_pemesanan;?></td>
    <td><?=$datap->nama_pelanggan;?></td>
    <td>
    <?php
    	foreach($data_menu as $datam)
		{
	?>
	<?=$datam->nama_menu;?>
    </td>
    <?php
		}
	?>
    <td><?=$datap->qty;?></td>
    <td><?=$datap->total;?></td>
 </tr>
 <?php
	}
 ?>
</table>
 </div>
</div>
</body>
</html>