<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class karyawan_model extends CI_Model 
{
	private $_table = "master_karyawan";
	
	public function tampilDataKaryawan()
	{
		$query = $this->db->query("SELECT * FROM master_karyawan WHERE flag = 1");
		return $query->result();
	}
	
	public function save()
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $bln . "-" . $bln . "-" . $tgl;
		
		$data['nik'] = $this->input->post('nik');
		$data['nama'] = $this->input->post('nama');
		$data['alamat'] = $this->input->post('alamat');
		$data['telp'] = $this->input->post('tlp');
		$data['tempat_lahir'] = $this->input->post('tempat_lahir');
		$data['tanggal_lahir'] = $tgl_gabung;
		$data['flag'] = 1;
		
		$this->db->insert($this->_table, $data);
	}
	public function edit($nik)
	{
		$query = $this->db->query("SELECT * FROM master_karyawan WHERE nik = $nik and flag = 1");
		return $query->result();
	}
	public function update($nik)
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;
		
		$data['nik'] = $this->input->post('nik');
		$data['nama'] = $this->input->post('nama');
		$data['alamat'] = $this->input->post('alamat');
		$data['telp'] = $this->input->post('tlp');
		$data['tempat_lahir'] = $this->input->post('tempat_lahir');
		$data['tanggal_lahir'] = $tgl_gabung;
		$data['flag'] = 1;
		
		$this->db->where('nik',$nik);
		$this->db->update($this->_table, $data);
	}
	public function delete($nik)
	{
		$this->db->where('nik', $nik);
		$this->db->delete($this->_table);
	}
}