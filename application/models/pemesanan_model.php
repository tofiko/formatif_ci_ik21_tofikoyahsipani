<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pemesanan_model extends CI_Model 
{
	private $_table = "transaksi_pemesanan";
	
	public function tampilDataPemesanan()
	{
		$query = $this->db->query("SELECT * FROM transaksi_pemesanan");
		return $query->result();
	}
	public function save()
	{
		$id_pemesanan >=1;
		$id_pemesanan++;
		$id=$this->input->post($id_pemesanan);
		$pegawai            = $this->input->post('nik');
        $nama_pelanggan     = $this->input->post('nama_pelanggan');
        $kode_menu          = $this->input->post('kode_menu');
        $qty                = $this->input->post('qty');
		$hargamenu          = $this->menu_model->cariHargaMenu($kode_menu);
		
		$data['id_pemesanan']	= $id;		
		$data['nik']			= $pegawai;
		$data['tgl_pemesanan']	= date('Y-m-d');
		$data['nama_pelanggan']	= $nama_pelanggan;
		$data['kode_menu']		= $kode_menu;
		$data['qty']			= $qty;
		$data['total']	= $qty * $hargamenu;
		
		$this->db->insert($this->_table, $data);
	}

}