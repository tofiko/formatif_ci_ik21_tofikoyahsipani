<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class karyawan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("karyawan_model");
	}
	public function index()
	{
		$this->listkaryawan();
	}
	public function listkaryawan()
	{
		$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		$this->load->view('list_karyawan',$data);
	}
	public function input()
	{	
		$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		if (!empty($_REQUEST))
		{
			$m_karyawan = $this->karyawan_model;
			$m_karyawan->save();
			redirect("karyawan/index", "refresh");	
		}
		
		$this->load->view('input_karyawan',$data);	
	}
	public function editkaryawan($nik)
	{
		$data['edit_karyawan'] = $this->karyawan_model->edit($nik);
		
		if (!empty($_REQUEST))
		{
			$m_karyawan = $this->karyawan_model;
			$m_karyawan->update($nik);
			redirect("karyawan/index", "refresh");	
		}
		
		$this->load->view('edit_karyawan',$data);
	}
	public function deletekaryawan($nik)
	{
			$m_karyawan = $this->karyawan_model;
			$m_karyawan->delete($nik);
			redirect("karyawan/index", "refresh");	
	}
}