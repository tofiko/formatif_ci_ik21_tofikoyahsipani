<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class menu extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
			
	}
	public function index()
	{
		$this->listmenu();	
	}
	public function listmenu()
	{
		$data['data_menu'] = $this->menu_model->tampilDataMenu();
		$this->load->view('list_menu',$data);	
	}
	public function input()
	{	
		$data['data_menu'] = $this->menu_model->tampilDataMenu();
		if (!empty($_REQUEST))
		{
			/*cek lemparan data
			var_dump($this->input->post()); die();*/
			$m_menu = $this->menu_model;
			$m_menu->save();
			redirect("menu/index", "refresh");	
		}
		
		$this->load->view('input_menu',$data);	
	}
	public function editmenu($kode_menu)
	{
		$data['edit_menu'] = $this->menu_model->edit($kode_menu);
		
		if (!empty($_REQUEST)) 
		{
			$m_menu = $this->menu_model;
			$m_menu->update($kode_menu);
			redirect("menu/listmenu", "refresh");
		}
		$this->load->view('edit_menu', $data);
	}

	public function deletemenu($kode_menu)
	{
			$m_menu = $this->menu_model;
			$m_menu->delete($kode_menu);
			redirect("menu/index", "refresh");	
	}
}
?>