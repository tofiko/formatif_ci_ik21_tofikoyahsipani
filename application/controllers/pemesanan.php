<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pemesanan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("pemesanan_model");
			
	}
	public function index()
	{
		$this->listpemesanan();	
	}
	public function listpemesanan()
	{
		$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		$data['data_menu'] = $this->menu_model->tampilDataMenu();
		$data['data_pemesanan'] = $this->pemesanan_model->tampilDataPemesanan();
		$this->load->view('list_pemesanan',$data);	
	}
	public function input()
	{	
		$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		$data['data_menu'] = $this->menu_model->tampilDataMenu();
		if (!empty($_REQUEST))
		{
			$m_menu = $this->pemesanan_model;
			$m_menu->save();
			redirect("pemesanan/index", "refresh");	
		}
		
		$this->load->view('input_pemesanan',$data);	
	}
}
?>